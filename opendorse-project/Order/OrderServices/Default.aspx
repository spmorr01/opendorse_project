﻿<%@ Page Title="Order Services - LawnHiro (Partial)" Language="C#" MasterPageFile="~/LawnHiro.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="opendorse_project.Order.OrderServices.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxJqkqEcfHvornc9l38rrrZ53iux1X2lY&v=3.exp&sensor=false&libraries=places"></script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <script src="/Scripts/page-scripts/order-services.js"></script>
    <script src="/Scripts/page-scripts/jquery-bootstrap-modal-steps.js"></script>
    
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/Content/page-css/order.css" />

    <script type="text/javascript">
        $(function () {
            Initialize();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="outer">
        <div class="middle">
            <div class="inner">
                <form autocomplete="on">
                    <div>
                        <input type="text" id="addressInput" style="width: 100%; text-align: center;" />
                    </div>
                    <div style="padding: 5px 5px;">
                        <input class="btn btn-primary" type="button" onclick="addOrder();" value="See Price" />
                    </div>

                    <!-- Button trigger modal -->
                    <button style="display: none" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" id="btnLaunchOrderModal">Launch demo modal </button>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="js-title-step"></h4>
                </div>
                <div class="modal-body">
                    <div class="row hide" data-step="1" data-title="Order Information">
                        <div class="well" style="background-color: white; border: none; box-shadow: none; margin-bottom: 0; padding-top: 0; padding-bottom: 0;">
                            <form autocomplete="on">
                                <div class="container-fluid form-group">
                                    <div class="row" style="border-bottom: solid; margin: 5px 0px;">
                                        <div class="col-xs-12" style="font-weight: bold; font-size: 24px; text-align: center;">
                                            Price: <span id="orderAmount" data-bind="text: '$' + ko.unwrap(orderVM.finalPrice)"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label for="orderFirstName">First Name*:</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <input class="form-control" id="orderFirstName" name="firstname" type="text" data-bind="value: orderVM.firstName" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label for="orderLasttName">Last Name*:</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <input class="form-control" id="orderLastName" name="lastname" type="text" data-bind="value: orderVM.lastName" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label for="orderEmail">Email Address*:</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <input class="form-control" id="orderEmail" name="email" type="text" data-bind="value: orderVM.email" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label for="orderCustomerNotes">Order Notes:</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <input class="form-control" id="orderCustomerNotes" type="text" placeholder="Ex: Please trim around the fence" data-bind="value: orderVM.customerNotes" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label for="orderBusinessSource">How did you hear about us?*:</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <select class="form-control" id="orderBusinessSource" data-bind="value: orderVM.businessSource">
                                                <option value="Facebook">Facebook</option>
                                                <option value="Google">Google</option>
                                                <option value="Word of Mouth">Word of Mouth</option>
                                                <option value="Website">Website</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label for="txtCouponCode">Coupon Code:</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-xs-9">
                                                        <input class="form-control" id="txtCouponCode" type="text" data-bind="value: orderVM.couponCode" />
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input class="btn btn-primary" id="btnCouponCode" type="button" value="Apply" data-bind="click: applyCouponCode.bind($data)" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row hide" data-step="2" data-title="Terms and Conditions">
                        <div class="well" style="background-color: white; border: none; box-shadow: none; margin-bottom: 0; padding-top: 0; padding-bottom: 0; text-align: center;">
                            <div>
                                <label for="termsAndConditions">
                                    <input type="checkbox" id="termsAndConditions" />
                                    Agree to <a target="_new" href="http://www.lawnhiro.com/#!terms-and-conditions/x6s6o">terms &amp; conditions</a>
                                </label>
                            </div>
                            <div id="paypal-button">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal"></button>
                    <button type="button" class="btn btn-warning js-btn-step" data-orientation="previous"></button>
                    <button type="button" class="btn btn-success js-btn-step" data-orientation="next"></button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#myModal').modalSteps();
    </script>
    <style>
        .Spinner {
            background: #ffffff;
            opacity: 0.8;
            color: #666666;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
        }
    </style>
    <div id="loadingSpinner" class="Spinner" style="display: none;">
        <img src="/loader.gif" alt="Loading" /><br />
        Loading...
    </div>
</asp:Content>
