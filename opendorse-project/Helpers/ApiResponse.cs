﻿using opendorse_project.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.Helpers
{
    public class ApiResponse
    {
        public class ApiDataResponse
        {
            public Dictionary<string, object> data { get; set; }

            public ApiDataResponse() { }

            public ApiDataResponse(Dictionary<string, object> value)
            {
                data = value;
            }
        }

        public static Dictionary<string, object> Response(bool success, object data)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response.Add(LawnHiroConstants.ApiResponseConstants.SuccessKey, false);
            response.Add(LawnHiroConstants.ApiResponseConstants.MessageKey, LawnHiroConstants.ApiResponseConstants.ErrorMessage);
            response.Add(LawnHiroConstants.ApiResponseConstants.DataKey, null); // TODO: change this data key, so when we receive the response in AJAX, it's not response.data.data

            if (success)
            {
                response[LawnHiroConstants.ApiResponseConstants.SuccessKey] = true;
                response[LawnHiroConstants.ApiResponseConstants.MessageKey] = LawnHiroConstants.ApiResponseConstants.SuccessMessage;
                response[LawnHiroConstants.ApiResponseConstants.DataKey] = data;
            }

            return response;
        }
    }
}