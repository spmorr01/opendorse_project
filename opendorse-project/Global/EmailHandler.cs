﻿using opendorse_project.BLL;
using opendorse_project.Constants;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace opendorse_project.Global
{
    public class EmailHandler
    {

        public static bool EmailClaimedOrderNotification(int orderId, int providerId, DateTime eta)
        {
            try
            {
                Orders.ClaimedOrders orderInformation = new Orders.ClaimedOrders();
                Providers.ProviderInfo providerInformation = new Providers.ProviderInfo();

                orderInformation = OrdersManager.GetClaimedOrderInfomation(orderId);
                providerInformation = ProviderManager.GetProviderInformation(providerId);

                if (orderInformation != null)
                {
                    var fromAddress = new MailAddress("order.lawnhiro@gmail.com", "LawnHiro Order Status");
                    var toAddress = new MailAddress(orderInformation.Email, $"{orderInformation.FirstName} {orderInformation.LastName}");
                    const string fromPassword = "lawnhiro1";
                    const string subject = "Your Order Has Been Claimed!";

                    string displayPhotoId = "DisplayPhoto";
                    Attachment displayPhotoAttachment = CreateNewPhotoAttachment(displayPhotoId, "provider_display_photo", providerInformation.Headshot);

                    string mowerId = "Mower";
                    Attachment mowerAttachment = CreateNewPhotoAttachment(mowerId, "provider_mower_photo", providerInformation.Mower);


                    string body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailNotifications/ClaimedOrderTemplate.html"));
                    body = body.Replace(LawnHiroConstants.ClaimOrderEmailNotification.ETA, $"{eta.ToShortDateString()} { eta.ToShortTimeString()}");
                    body = body.Replace(LawnHiroConstants.ClaimOrderEmailNotification.LawnhiroInfo, $"{providerInformation.FirstName}, {providerInformation.City}, {providerInformation.State}");
                    body = body.Replace(LawnHiroConstants.ClaimOrderEmailNotification.DisplayPhotoId, displayPhotoId);
                    body = body.Replace(LawnHiroConstants.ClaimOrderEmailNotification.MowerId, mowerId);

                    MailMessage message = new MailMessage();
                    message.From = fromAddress;
                    message.To.Add(toAddress);
                    message.Subject = subject;
                    message.IsBodyHtml = true;
                    message.Body = body;
                    message.Attachments.Add(displayPhotoAttachment);
                    message.Attachments.Add(mowerAttachment);

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };

                    smtp.Send(message);
                }
                return true;
            }
            catch (Exception ex)
            {
                EmailException(ex, orderId, providerId);
                return true;
            }
        }

        public static bool EmailFinishedOrderNotification(int orderId)
        {

            Orders.CompletedOrders orderInformation = new Orders.CompletedOrders();
            try
            {
                orderInformation = OrdersManager.GetCompletedOrderInfomation(orderId);

                List<string> fileIds = new List<string> { "OrderPhoto1", "OrderPhoto2" };
                Dictionary<int, byte[]> imagesForAttachments = new Dictionary<int, byte[]> { { 0, orderInformation.CompletedPhoto1 }, { 1, orderInformation.CompletedPhoto2 } };

                if (orderInformation.CompletedPhoto3 != null) fileIds.Add("OrderPhoto3"); imagesForAttachments.Add(2, orderInformation.CompletedPhoto3);

                if (orderInformation != null)
                {
                    var fromAddress = new MailAddress("order.lawnhiro@gmail.com", "LawnHiro Order Status");
                    var toAddress = new MailAddress(orderInformation.Email, $"{orderInformation.FirstName} {orderInformation.LastName}");
                    const string fromPassword = "lawnhiro1";
                    const string subject = "Your Order Has Been Completed!";

                    string body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailNotifications/CompletedOrderTemplate.html"));
                    string imageDiv = CreateOrderImageDiv(fileIds, orderInformation);
                    body = body.Replace("###CompletedOrderImages###", imageDiv);

                    MailMessage message = new MailMessage();
                    message.From = fromAddress;
                    message.To.Add(toAddress);
                    message.Subject = subject;
                    message.IsBodyHtml = true;
                    message.Body = body;

                    AddCompletedPhotoAttachments(imagesForAttachments, fileIds, ref message);

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };

                    smtp.Send(message);
                }

                return true;
            }
            catch (Exception ex)
            {
                EmailException(ex, orderId, 0);
                return true;
            }
        }

        public static void EmailReceivedNotification(Orders.NewOrder newOrderData)
        {
            try
            {

                var fromAddress = new MailAddress("order.lawnhiro@gmail.com", "LawnHiro Order Status");
                var toAddress = new MailAddress(newOrderData.Email, $"{newOrderData.FirstName} {newOrderData.LastName}");
                const string fromPassword = "lawnhiro1";
                const string subject = "Your Order Has Been Received!";

                string body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailNotifications/OrderReceivedTemplate.html"));

                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.CustomerName, $"{newOrderData.FirstName} {newOrderData.LastName}");
                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.CustomerEmail, newOrderData.Email);

                string customerNotes = newOrderData.CustomerNotes != null ? newOrderData.CustomerNotes : "None Provided";

                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.CustomerNotes, $"{customerNotes}");

                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.StreetAddress, newOrderData.Address1);
                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.City, newOrderData.City);
                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.State, newOrderData.State);
                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.Zip, newOrderData.Zip);

                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.OrderPrice, String.Format("{0:C2}", newOrderData.Price));
                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.OrderDate, DateTime.UtcNow.ToUniversalTime().ToString("MMMM d, yyyy"));
                body = body.Replace(LawnHiroConstants.OrderReceivedEmailNotification.TransactionID, newOrderData.PayPalOrderID);

                MailMessage message = new MailMessage();
                message.From = fromAddress;
                message.To.Add(toAddress);
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                smtp.Send(message);
            }
            catch (Exception ex)
            {
                EmailException(ex, newOrderData);
            }
        }

        public static void EmailReceivedOrderForProvidersNotification(Orders.NewOrder newOrderData)
        {
            try
            {
                List<Providers.EmailReceivedNotifications> providersForEmail = ProviderManager.GetProvidersToSendOrderNotificationTo(newOrderData.City);

                foreach (var provider in providersForEmail)
                {
                    var fromAddress = new MailAddress("order.lawnhiro@gmail.com", "LawnHiro Order Manager");
                    var toAddress = new MailAddress(provider.Email);
                    string mapAddressMarkerParam = HttpUtility.HtmlEncode($"{newOrderData.Address1}, {newOrderData.City}, {newOrderData.State} {newOrderData.Zip}");
                    string staticMapUrl = $"https://maps.googleapis.com/maps/api/staticmap?markers={mapAddressMarkerParam}&zoom=14&size=750x400&key=AIzaSyATiMiFCSPiuHmHGwBrBhGOEmH3wadkXhM";
                    const string fromPassword = "lawnhiro1";
                    const string subject = "New Order In Your Area!";
                    string orderNotes = newOrderData.CustomerNotes ?? "None Provided";

                    string body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailNotifications/NewOrderForProviderTemplate.html"));
                    body = body.Replace(LawnHiroConstants.OrderReceivedForProviderEmailNotification.CityStateHeader, $"{newOrderData.City}, {newOrderData.State}");
                    body = body.Replace(LawnHiroConstants.OrderReceivedForProviderEmailNotification.StreetAddress, $"{newOrderData.Address1}");
                    body = body.Replace(LawnHiroConstants.OrderReceivedForProviderEmailNotification.CityStateZip, $"{newOrderData.City}, {newOrderData.State} {newOrderData.Zip}");
                    body = body.Replace(LawnHiroConstants.OrderReceivedForProviderEmailNotification.Earnings, $"${Decimal.Multiply(newOrderData.Price, 0.7m).ToString("0.00")}");
                    body = body.Replace(LawnHiroConstants.OrderReceivedForProviderEmailNotification.OrderNotes, orderNotes);
                    body = body.Replace(LawnHiroConstants.OrderReceivedForProviderEmailNotification.StaticMapImage, staticMapUrl);


                    MailMessage message = new MailMessage();
                    message.From = fromAddress;
                    message.To.Add(toAddress);
                    message.Subject = subject;
                    message.IsBodyHtml = true;
                    message.Body = body;

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };

                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                EmailException(ex, newOrderData);
            }
        }

        public static bool EmailRescheduledOrderNotification(Orders.RescheduleInformationForOrder rescheduledOrderInfo, int providerId)
        {
            try
            {
                Orders.ClaimedOrders orderInformation = new Orders.ClaimedOrders();
                Providers.ProviderInfo providerInformation = new Providers.ProviderInfo();

                orderInformation = OrdersManager.GetClaimedOrderInfomation(rescheduledOrderInfo.OrderId);
                providerInformation = ProviderManager.GetProviderInformation(providerId);

                if (orderInformation != null)
                {
                    var fromAddress = new MailAddress("order.lawnhiro@gmail.com", "LawnHiro Order Status");
                    var toAddress = new MailAddress(orderInformation.Email, $"{orderInformation.FirstName} {orderInformation.LastName}");
                    const string fromPassword = "lawnhiro1";
                    const string subject = "Your Order Has Been Rescheduled";

                    string body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailNotifications/RescheduledOrderTemplate.html"));
                    body = body.Replace("###NEWTIME###", $"{rescheduledOrderInfo.NewTime.ToShortDateString()} { rescheduledOrderInfo.NewTime.ToShortTimeString()}");
                    body = body.Replace("###PROVIDERNAME###", providerInformation.FirstName);
                    body = body.Replace("###REASON###", rescheduledOrderInfo.Reason);

                    MailMessage message = new MailMessage();
                    message.From = fromAddress;
                    message.To.Add(toAddress);
                    message.Subject = subject;
                    message.IsBodyHtml = true;
                    message.Body = body;

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };

                    smtp.Send(message);
                }
                return true;
            }
            catch (Exception ex)
            {
                EmailException(ex, rescheduledOrderInfo.OrderId, providerId);
                return true;
            }
        }

        private static void EmailException(Exception ex, Orders.NewOrder newOrderData)
        {
            var fromAddress = new MailAddress("order.lawnhiro@gmail.com", "LawnHiro Debugger");
            var toAddress = new MailAddress("spencermorris22@gmail.com", "Spencer Morris");
            const string fromPassword = "lawnhiro1";
            const string subject = "EXCEPTION CAUGHT!";

            MailMessage message = new MailMessage();
            message.From = fromAddress;
            message.To.Add(toAddress);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = $"Exception caught:<br/><br/>{ex.ToString()}<br/><br/>Stack trace:<br/><br/>{ex.StackTrace.ToString()}<br/><br/>Order: {newOrderData}";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            smtp.Send(message);
        }

        private static void EmailException(Exception ex, int orderId, int providerId)
        {
            var fromAddress = new MailAddress("order.lawnhiro@gmail.com", "LawnHiro Debugger");
            var toAddress = new MailAddress("spencermorris22@gmail.com", "Spencer Morris");
            const string fromPassword = "lawnhiro1";
            const string subject = "EXCEPTION CAUGHT!";

            MailMessage message = new MailMessage();
            message.From = fromAddress;
            message.To.Add(toAddress);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = $"Exception caught:<br/><br/>{ex.ToString()}<br/><br/>Stack trace:<br/><br/>{ex.StackTrace.ToString()}<br/><br/>Order ID: {orderId}<br/>Provider ID: {providerId}";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            smtp.Send(message);
        }

        private static string CreateOrderImageDiv(List<string> fileIds, Orders.CompletedOrders orderInformation)
        {
            string imageDiv = string.Empty;
            foreach (string fileId in fileIds)
            {
                imageDiv = imageDiv + $"<div><img style='height: 350px; width: auto; padding: 5px 5px;' src='cid:{fileId}' alt='Order Photo'/> </div>";
            }
            return imageDiv;
        }

        private static void AddCompletedPhotoAttachments(Dictionary<int, byte[]> imagesForAttachments, List<string> fileIds, ref MailMessage emailMessage)
        {
            for (int i = 0; i < fileIds.Count; i++)
            {
                emailMessage.Attachments.Add(CreateNewPhotoAttachment(fileIds[i], $"order_photo_{i}", imagesForAttachments[i]));
            }
        }

        private static Attachment CreateNewPhotoAttachment(string attachmentId, string attachmentName, byte[] image)
        {
            Attachment imageAttachment = new Attachment(new MemoryStream(image), attachmentName);
            imageAttachment.ContentId = attachmentId;
            imageAttachment.ContentDisposition.Inline = true;
            imageAttachment.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
            imageAttachment.ContentType.MediaType = "image/*";

            return imageAttachment;
        }
    }
}