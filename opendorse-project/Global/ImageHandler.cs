﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace opendorse_project.Global
{
    public class ImageHandler
    {
        public static byte[] GetImageBytes(HttpPostedFile file)
        {
            byte[] binaryData;

            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                binaryData = binaryReader.ReadBytes(file.ContentLength);
            }

            return binaryData;
        }
    }
}