﻿using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace opendorse_project.Global
{
    public static class ZillowHandler
    {
        public static Dictionary<string, decimal> GetZillowData(ZillowAddress zillowParam)
        {
            Dictionary<string, decimal> response = new Dictionary<string, decimal>();

            string addressParam = HttpUtility.UrlEncode(zillowParam.Address);
            string cityStateZipParam = HttpUtility.UrlEncode($"{zillowParam.City}, {zillowParam.State} {zillowParam.Zip}");
            string ZWS_ID = "X1-ZWz1fafu2i84y3_1hdll";

            XmlDocument xml = new XmlDocument();
            xml.Load($"http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id={ZWS_ID}&address={addressParam}&citystatezip={cityStateZipParam}");
            XmlNode resultsNode = xml.DocumentElement.SelectSingleNode("response/results/result");
            if (resultsNode == null) // no zillow info found
            {
                response["CalculatedResultArea"] = 0;
                response["CalculatedResultPrice"] = 0;
                return response;
            }
            else
            {
                XmlElement lotSqFtNode = resultsNode["lotSizeSqFt"];
                decimal? lotSqFt, finishedSqFt;
                if (lotSqFtNode == null)
                {
                    lotSqFt = null;
                }
                else
                {
                    lotSqFt = decimal.Parse(lotSqFtNode.InnerText);
                }
                XmlElement finishedSqFtNode = resultsNode["finishedSqFt"];
                if (finishedSqFtNode == null)
                {
                    finishedSqFt = null;
                }
                else
                {
                    finishedSqFt = decimal.Parse(finishedSqFtNode.InnerText);
                }
                int zpid = int.Parse(resultsNode["zpid"].InnerText);
                XmlDocument updatedPropertyDetails = new XmlDocument();
                updatedPropertyDetails.Load($"http://www.zillow.com/webservice/GetUpdatedPropertyDetails.htm?zws-id={ZWS_ID}&zpid={zpid}");
                XmlNode numFloorsNode = updatedPropertyDetails.DocumentElement.SelectSingleNode("numFloors");
                decimal? numFloors;
                if (numFloorsNode == null)
                {
                    numFloors = null;
                }
                else
                {
                    numFloors = decimal.Parse(numFloorsNode.InnerText);
                }

                return GetResidenceInfo(zpid, finishedSqFt, lotSqFt, numFloors);
            }
        }

        private static Dictionary<string, decimal> GetResidenceInfo(int zillowId, decimal? zFinishedSqFt, decimal? zLotSizeSqFt, decimal? zNumberOfFloors)
        {
            if (zillowId == 0)
            {
                throw new ArgumentNullException("Zillow Information");
            }
            Dictionary<string, decimal> response = new Dictionary<string, decimal>();

            decimal estimatedFootprint;
            decimal lotSqFt;
            decimal finalFinishedSqFt = zFinishedSqFt ?? 2600;
            if (zLotSizeSqFt == null)
            {
                lotSqFt = finalFinishedSqFt * 3;
            }
            else
            {
                lotSqFt = (decimal)zLotSizeSqFt;
            }
            if (zNumberOfFloors == null) // actual number of floors not available, must estimate.
            {
                estimatedFootprint = Math.Min(finalFinishedSqFt, lotSqFt / 2);
            }
            else
            {
                estimatedFootprint = (finalFinishedSqFt / (decimal)zNumberOfFloors);
            }
            decimal mowableSqFt = lotSqFt - estimatedFootprint;
            decimal finalPrice = CalculatePrice(mowableSqFt, 25, 0.0023m);

            response["CalculatedResultArea"] = mowableSqFt;
            response["CalculatedResultPrice"] = finalPrice;

            return response;
        }

        public static decimal CalculatePrice(decimal mowableSqFt, decimal basePrice, decimal pricePerSqFt)
        {
            decimal price = Math.Floor(basePrice + (mowableSqFt * pricePerSqFt));
            return price;
        }
    }
}