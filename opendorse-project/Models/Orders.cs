﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.Models
{
    public class Orders
    {
        public class NewOrder
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string BusinessSource { get; set; }
            public string PayPalOrderID { get; set; }
            public double CalculationArea { get; set; }
            public decimal Price { get; set; }
            public string CustomerNotes { get; set; }
            public bool CouponUsed { get; set; }
            public string CouponCode { get; set; }
        }

        public class ActiveOrders
        {
            public int OrderID { get; set; }
            public DateTime Placed { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public decimal Price { get; set; }
            public string CustomerNotes { get; set; }
        }

        public class ClaimedOrders
        {
            public int OrderID { get; set; }
            public DateTime Placed { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public decimal Price { get; set; }
            public string CustomerNotes { get; set; }
            public DateTime? ETA { get; set; }
        }

        public class CompletedOrders
        {
            public int OrderID { get; set; }
            public DateTime Placed { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public decimal Price { get; set; }
            public string CustomerNotes { get; set; }
            public DateTime? ETA { get; set; }
            public string ProviderNotes { get; set; }
            public byte[] CompletedPhoto1 { get; set; }
            public byte[] CompletedPhoto2 { get; set; }
            public byte[] CompletedPhoto3 { get; set; }
        }

        public class AdminClaimedOrders
        {
            public int OrderID { get; set; }
            public string OrderFirstName { get; set; }
            public string OrderLastName { get; set; }
            public string OrderEmail { get; set; }
            public string OrderAddress1 { get; set; }
            public string OrderAddress2 { get; set; }
            public string OrderCity { get; set; }
            public string OrderState { get; set; }
            public string OrderZip { get; set; }
            public decimal Price { get; set; }
            public string CustomerNotes { get; set; }
            public DateTime Placed { get; set; }
            public DateTime ETA { get; set; }
            public int ProviderID { get; set; }
            public string ProviderFirstName { get; set; }
            public string ProviderLastName { get; set; }
        }

        public class HistoryProviderClaimedOrdersSelect
        {
            public string OrderFirstName { get; set; }
            public string OrderLastName { get; set; }
            public string OrderAddress1 { get; set; }
            public string OrderAddress2 { get; set; }
            public string OrderCity { get; set; }
            public string OrderState { get; set; }
            public string OrderZip { get; set; }
            public decimal Price { get; set; }
            public string CustomerNotes { get; set; }
            public DateTime DateCompleted { get; set; }
            public bool ProviderPaid { get; set; }
        }

        public class HistoryProviderClaimedOrders
        {
            public string OrderName { get; set; }
            public string OrderAddress { get; set; }
            public decimal Price { get; set; }
            public string CustomerNotes { get; set; }
            public DateTime DateCompleted { get; set; }
            public bool ProviderPaid { get; set; }
        }

        public class ClaimInformationForOrder
        {
            public int OrderId { get; set; }

            public DateTime ETA { get; set; }
        }

        public class RescheduleInformationForOrder
        {
            public int OrderId { get; set; }

            public DateTime NewTime { get; set; }

            public string Reason { get; set; }
        }

        public class CompletedInformationForOrder
        {
            public int OrderId { get; set; }

            public byte[] File1 { get; set; }

            public byte[] File2 { get; set; }

            public byte[] File3 { get; set; }

            public string ProviderNotes { get; set; }
        }
    }
}