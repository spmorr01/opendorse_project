﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.Models
{
    public class Providers
    {
        public class ProviderInfo
        {
            public int Id { get; set; }
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string Phone { get; set; }
            public bool Admin { get; set; }
            public byte[] Headshot { get; set; }
            public byte[] Mower { get; set; }
        }

        public class ProviderRegistration
        {
            public int Id { get; set; }

            public string Email { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string Address1 { get; set; }

            public string Address2 { get; set; }

            public string City { get; set; }

            public string State { get; set; }

            public string Zip { get; set; }

            public string Phone { get; set; }

            public string SuppliedPassword { get; set; }

            public byte[] PasswordSalt { get; set; }

            public byte[] PasswordHash { get; set; }

            public bool Active { get; set; }

            public bool Admin { get; set; }

            public string CouponCode { get; set; }

            public string PayPalEmail { get; set; }

            public bool Confirmed { get; set; }

            public byte[] Headshot { get; set; }

            public byte[] Mower { get; set; }

            public DateTime SignedAgreement { get; set; }
        }

        public class AdminProviderCompletedOrderHistory
        {
            public int OrderID { get; set; }
            public decimal OrderPrice { get; set; }
            public string ProviderNotes { get; set; }
            public bool ProviderPaid { get; set; }
            public int ProviderID { get; set; }
            public string ProviderFirstName { get; set; }
            public string ProviderLastName { get; set; }
            public string ProviderEmail { get; set; }
            public DateTime DateCompleted { get; set; }
        }

        public class ActiveProviders
        {
            public int Id { get; set; }
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
            public string Phone { get; set; }
        }

        public class EmailReceivedNotifications
        {
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }
    }
}