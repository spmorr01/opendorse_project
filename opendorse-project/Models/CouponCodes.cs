﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.Models
{
    public class CouponCodes
    {
        public class CouponCode
        {
            public string Code { get; set; }
            public int TimesUsed { get; set; }
            public bool Enabled { get; set; }
            public decimal Discount { get; set; }
        }

        public class AppliedCouponCode
        {
            public decimal NewPrice { get; set; }
            public bool Valid { get; set; }
        }
    }
}