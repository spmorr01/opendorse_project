﻿using opendorse_project.BLL;
using opendorse_project.Helpers;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace opendorse_project.Controllers
{
    [RoutePrefix("api/CustomerOrders")]
    public class CustomerOrdersController : ApiController
    {
        // TODO: Either move session checker into a global method, or create a custom attribute to authorize all controller calls

        [Route("AddNewOrder")]
        [HttpPost]
        public ApiResponse.ApiDataResponse Post(Orders.NewOrder newOrderData)
        {
            return new ApiResponse.ApiDataResponse(ApiResponse.Response(OrdersManager.AddNewOrder(newOrderData), null));
        }

        [Route("ApplyCoupon")]
        [HttpPost]
        public ApiResponse.ApiDataResponse ApplyCoupon()
        {
            // TODO: Move this chunk into a class and create a method to get the data from the HttpContext like we do in other controllers
            string customerEmail = HttpContext.Current.Request.Form["CustomerEmail"];
            string couponCode = HttpContext.Current.Request.Form["CouponCode"];
            decimal price = decimal.Parse(HttpContext.Current.Request.Form["Price"]);

            return new ApiResponse.ApiDataResponse(ApiResponse.Response(true, CouponManager.ApplyCoupon(customerEmail, couponCode, price)));
        }
    }
}
