﻿using opendorse_project.Global;
using opendorse_project.Helpers;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace opendorse_project.Controllers
{
    [RoutePrefix("api/locationdata")]
    public class LocationDataController : ApiController
    {
        [Route("GetOrderInfoForLocation")]
        [HttpPost]
        public ApiResponse.ApiDataResponse GetOrderInfoForLocation(ZillowAddress zillowParam)
        {
            return new ApiResponse.ApiDataResponse(ApiResponse.Response(true, ZillowHandler.GetZillowData(zillowParam)));
        }
    }
}