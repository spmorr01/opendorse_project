﻿using opendorse_project.BLL;
using opendorse_project.Global;
using opendorse_project.Helpers;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace opendorse_project.Controllers
{
    [RoutePrefix("api/providerorders")]
    public class ProviderOrdersController : ApiController
    {
        // TODO: Either move session checker into a global method, or create a custom attribute to authorize all controller calls

        [Route("GetActiveOrders")]
        [HttpGet]
        public ApiResponse.ApiDataResponse GetAllOrders([FromUri] string cityFilter)
        {
            Dictionary<string, object> responseDict = new Dictionary<string, object>();

            string city = cityFilter == "undefined" || cityFilter == "All Cities" ? string.Empty : cityFilter;
            responseDict.Add("Orders", OrdersManager.GetActiveOrders(city));
            responseDict.Add("OrderCities", FiltersManager.GetCurrentOrderCities());
            responseDict.Add("SelectedCity", (string.IsNullOrEmpty(city) ? "All Cities" : city));

            return new ApiResponse.ApiDataResponse(ApiResponse.Response(true, responseDict));
        }

        [Route("ClaimOrder")]
        [HttpPut]
        public ApiResponse.ApiDataResponse ClaimOrder()
        {
            int providerId = 7;
            bool success = false;

            Orders.ClaimInformationForOrder claimInfo = OrdersManager.SetClaimInformationForOrderFromAPI(HttpContext.Current.Request);
            if (!OrdersManager.IsOrderClaimed(claimInfo.OrderId))
            {
                if (OrdersManager.ClaimOrder(claimInfo.OrderId, providerId, claimInfo.ETA))
                {
                    EmailHandler.EmailClaimedOrderNotification(claimInfo.OrderId, providerId, claimInfo.ETA);
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            else
            {
                success = false;
            }

            return new ApiResponse.ApiDataResponse(ApiResponse.Response(success, null));
        }

        [Route("RescheduleOrder")]
        [HttpPut]
        public ApiResponse.ApiDataResponse RescheduleOrder()
        {
            int providerId = 7;
            bool success = false;

            Orders.RescheduleInformationForOrder rescheduleInfo = OrdersManager.SetReschedulenformationForOrderFromAPI(HttpContext.Current.Request);
            if (OrdersManager.RescheduleOrder(rescheduleInfo))
            {
                EmailHandler.EmailRescheduledOrderNotification(rescheduleInfo, providerId);
                success = true;
            }
            else
            {
                success = false;
            }

            return new ApiResponse.ApiDataResponse(ApiResponse.Response(success, null));
        }


        [Route("GetClaimedOrders")]
        [HttpGet]
        public ApiResponse.ApiDataResponse GetClaimedOrders()
        {
            int providerId = 7;
            return new ApiResponse.ApiDataResponse(ApiResponse.Response(true, OrdersManager.GetProviderClaimedOrders(providerId)));
        }

        [Route("CompleteOrder")]
        [HttpPut]
        public ApiResponse.ApiDataResponse CompleteOrder()
        {
            bool success = false;
            Orders.CompletedInformationForOrder completeOrderInfo = OrdersManager.SetCompleteInformationForOrderFromAPI(HttpContext.Current.Request);

            if (OrdersManager.CompleteOrder(completeOrderInfo))
            {
                EmailHandler.EmailFinishedOrderNotification(completeOrderInfo.OrderId);
                success = true;
            }
            else
            {
                success = false;
            }

            return new ApiResponse.ApiDataResponse(ApiResponse.Response(success, null));
        }
    }
}