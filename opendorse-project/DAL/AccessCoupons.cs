﻿using opendorse_project.Entities;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.DAL
{
    public class AccessCoupons
    {
        // TODO: Add more specifics to Try/Catches, and return a more specific error to the end user
        // TODO: Create an error log

        public static bool CustomerUsedCoupon(string email, string couponCode)
        {
            LawnHiroTables.CouponsUsed couponUsed;
            using (var db = new LawnHiroDB())
            {
                try
                {
                    couponUsed = (from c in db.CouponsUsed
                                  where c.CouponCode == couponCode && c.CustomerEmail == email
                                  select c).FirstOrDefault();
                }
                catch
                {
                    throw;
                }
            }
            return couponUsed != null;
        }
        public static CouponCodes.CouponCode GetCouponInfo(string couponCode)
        {
            CouponCodes.CouponCode coupon = new CouponCodes.CouponCode();
            using (var db = new LawnHiroDB())
            {
                try
                {
                    coupon = (from c in db.CouponCodes
                              where c.Code == couponCode
                              select new CouponCodes.CouponCode()
                              {
                                  Code = c.Code,
                                  TimesUsed = c.TimesUsed,
                                  Enabled = c.Enabled,
                                  Discount = c.Discount
                              }).FirstOrDefault();
                }
                catch
                {
                    throw;
                }
            }
            return coupon;
        }
    }
}