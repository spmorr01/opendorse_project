﻿using opendorse_project.Entities;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.DAL
{
    public class AccountAccess
    {
        // TODO: Add more specifics to Try/Catches, and return a more specific error to the end user
        // TODO: Create an error log
       

        public static Providers.ProviderInfo GetProviderInformation(string email)
        {
            List<Providers.ProviderInfo> provider = null;

            using (var db = new LawnHiroDB())
            {
                provider = (from p in db.Provider
                            where p.Email == email
                            select new Providers.ProviderInfo
                            {
                                Id = p.Id,
                                Email = p.Email,
                                FirstName = p.FirstName,
                                LastName = p.LastName,
                                Address1 = p.AddressLine1,
                                Address2 = p.AddressLine2,
                                City = p.City,
                                State = p.State,
                                Zip = p.State,
                                Phone = p.PhoneNumber,
                                Admin = p.Admin
                            }).ToList();
            }
            return provider.First();
        }

        public static Providers.ProviderInfo GetProviderInformation(int providerId)
        {
            List<Providers.ProviderInfo> provider = null;

            using (var db = new LawnHiroDB())
            {
                provider = (from p in db.Provider
                            where p.Id == providerId
                            select new Providers.ProviderInfo
                            {
                                Id = p.Id,
                                Email = p.Email,
                                FirstName = p.FirstName,
                                LastName = p.LastName,
                                Address1 = p.AddressLine1,
                                Address2 = p.AddressLine2,
                                City = p.City,
                                State = p.State,
                                Zip = p.State,
                                Phone = p.PhoneNumber,
                                Admin = p.Admin,
                                Headshot = p.Headshot,
                                Mower = p.Mower
                            }).ToList();
            }
            return provider.First();
        }

        public static List<Providers.EmailReceivedNotifications> GetProvidersToSendOrderNotificationTo(List<string> city)
        {
            List<Providers.EmailReceivedNotifications> providers = null;

            using (var db = new LawnHiroDB())
            {
                providers = (from p in db.Provider
                             where city.Contains(p.City) && p.Active == true
                             select new Providers.EmailReceivedNotifications
                             {
                                 Email = p.Email,
                                 FirstName = p.FirstName,
                                 LastName = p.LastName
                             }).ToList();
            }
            return providers;
        }
    }
}