﻿using opendorse_project.Entities;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.DAL
{
    public class AccessOrders
    {
        // TODO: Add more specifics to Try/Catches, and return a more specific error to the end user
        // TODO: Create an error log

        public static bool AddNewOrder(Orders.NewOrder newOrderData)
        {
            bool addSuccess;

            using (var db = new LawnHiroDB())
            {
                try
                {
                    var order = new LawnHiroTables.Orders
                    {
                        CustomerFirstName = newOrderData.FirstName,
                        CustomerLastName = newOrderData.LastName,
                        CustomerEmail = newOrderData.Email,
                        AddressLine1 = newOrderData.Address1,
                        AddressLine2 = newOrderData.Address2,
                        City = newOrderData.City,
                        State = newOrderData.State,
                        Zip = newOrderData.Zip,
                        Placed = DateTime.UtcNow.ToUniversalTime(),
                        PayPalOrderId = newOrderData.PayPalOrderID,
                        BusinessSource = newOrderData.BusinessSource,
                        CalculatedArea = newOrderData.CalculationArea,
                        Price = newOrderData.Price,
                        ETA = null,
                        CustomerNotes = newOrderData.CustomerNotes,
                        ClaimedBy = null,
                        Completed = false,
                        ProviderNotes = null,
                        CustomerRating = null,
                        ProviderPaid = false,
                        DateCompleted = null,
                        CouponUsed = newOrderData.CouponUsed ? newOrderData.CouponCode : null
                    };

                    db.Orders.Add(order);
                    db.SaveChanges();

                    if (newOrderData.CouponUsed)
                    {
                        db.Database.ExecuteSqlCommand($"UPDATE CouponCodes Set TimesUsed = TimesUsed + 1 WHERE Code = '{newOrderData.CouponCode}'");

                        var couponUsed = new LawnHiroTables.CouponsUsed
                        {
                            CustomerEmail = newOrderData.Email,
                            CouponCode = newOrderData.CouponCode
                        };

                        db.CouponsUsed.Add(couponUsed);
                        db.SaveChanges();
                    }

                    addSuccess = true;
                }
                catch
                {
                    addSuccess = false;
                }
            }

            return addSuccess;
        }

        public static IEnumerable<Orders.ActiveOrders> GetActiveOrders(string city)
        {
            IEnumerable<Orders.ActiveOrders> activeOrders = new List<Orders.ActiveOrders>();
            using (var db = new LawnHiroDB())
            {
                try
                {
                    activeOrders = (from o in db.Orders
                                    where o.ClaimedBy == null && o.Completed != true && (string.IsNullOrEmpty(city) ? 1 == 1 : o.City == city)
                                    select new Orders.ActiveOrders()
                                    {
                                        OrderID = o.Id,
                                        Placed = o.Placed,
                                        FirstName = o.CustomerFirstName,
                                        LastName = o.CustomerLastName,
                                        Email = o.CustomerEmail,
                                        Address1 = o.AddressLine1,
                                        Address2 = o.AddressLine2,
                                        City = o.City,
                                        State = o.State,
                                        Zip = o.Zip,
                                        Price = o.Price,
                                        CustomerNotes = o.CustomerNotes
                                    }).ToList();
                }
                catch
                {
                    throw;
                }
            }
            return activeOrders;
        }

        public static bool Claim(int orderId, int providerId, DateTime eta)
        {
            bool addSuccess;
            LawnHiroTables.Orders orders = new LawnHiroTables.Orders();
            try
            {
                using (var db = new LawnHiroDB())
                {
                    orders = (from o in db.Orders
                              where o.Id == orderId
                              select o).First();
                    orders.ClaimedBy = providerId;
                    orders.ETA = eta;
                    db.SaveChanges();
                }
                addSuccess = true;
            }
            catch
            {
                addSuccess = false;
            }

            return addSuccess;
        }

        public static bool CompleteOrder(Orders.CompletedInformationForOrder completeOrderInfo)
        {
            bool completeSuccess;
            LawnHiroTables.Orders orders = new LawnHiroTables.Orders();
            try
            {
                using (var db = new LawnHiroDB())
                {
                    orders = (from o in db.Orders
                              where o.Id == completeOrderInfo.OrderId
                              select o).First();
                    orders.Completed = true;
                    orders.DateCompleted = DateTime.UtcNow.ToUniversalTime();
                    orders.ProviderNotes = completeOrderInfo.ProviderNotes;
                    orders.CompletedPhoto1 = completeOrderInfo.File1;
                    orders.CompletedPhoto2 = completeOrderInfo.File2;
                    orders.CompletedPhoto3 = completeOrderInfo.File3;
                    db.SaveChanges();
                }
                completeSuccess = true;
            }
            catch
            {
                completeSuccess = false;
            }
            return completeSuccess;
        }

        public static bool RescheduleOrder(Orders.RescheduleInformationForOrder rescheduleOrderInfo)
        {
            bool rescheduleSuccess;
            LawnHiroTables.Orders orders = new LawnHiroTables.Orders();
            try
            {
                using (var db = new LawnHiroDB())
                {
                    orders = (from o in db.Orders
                              where o.Id == rescheduleOrderInfo.OrderId
                              select o).First();
                    orders.ETA = rescheduleOrderInfo.NewTime;
                    db.SaveChanges();
                }
                rescheduleSuccess = true;
            }
            catch
            {
                rescheduleSuccess = false;
            }
            return rescheduleSuccess;
        }

        public static bool IsOrderClaimed(int orderId)
        {
            bool orderIsClaimed;
            using (var db = new LawnHiroDB())
            {
                var result = from o in db.Orders
                             where o.Id == orderId && o.ClaimedBy != null
                             select o.Id;
                orderIsClaimed = result.Any();
            }
            return orderIsClaimed;
        }

        public static Orders.ClaimedOrders GetClaimedOrderInfomation(int orderId)
        {
            Orders.ClaimedOrders orderInformation = new Orders.ClaimedOrders();
            using (var db = new LawnHiroDB())
            {
                try
                {
                    orderInformation = (from o in db.Orders
                                        where o.Id == orderId
                                        select new Orders.ClaimedOrders()
                                        {
                                            OrderID = o.Id,
                                            FirstName = o.CustomerFirstName,
                                            LastName = o.CustomerLastName,
                                            Email = o.CustomerEmail,
                                            Address1 = o.AddressLine1,
                                            Address2 = o.AddressLine2,
                                            City = o.City,
                                            State = o.State,
                                            Zip = o.Zip,
                                            Price = o.Price,
                                            CustomerNotes = o.CustomerNotes,
                                            ETA = o.ETA
                                        }).First();
                }
                catch
                {
                    throw;
                }
            }
            return orderInformation;
        }

        public static Orders.CompletedOrders GetCompletedOrderInfomation(int orderId)
        {
            Orders.CompletedOrders orderInformation = new Orders.CompletedOrders();
            using (var db = new LawnHiroDB())
            {
                try
                {
                    orderInformation = (from o in db.Orders
                                        where o.Id == orderId
                                        select new Orders.CompletedOrders()
                                        {
                                            OrderID = o.Id,
                                            Placed = o.Placed,
                                            FirstName = o.CustomerFirstName,
                                            LastName = o.CustomerLastName,
                                            Email = o.CustomerEmail,
                                            Address1 = o.AddressLine1,
                                            Address2 = o.AddressLine2,
                                            City = o.City,
                                            State = o.State,
                                            Zip = o.Zip,
                                            Price = o.Price,
                                            CustomerNotes = o.CustomerNotes,
                                            ETA = o.ETA,
                                            ProviderNotes = o.ProviderNotes,
                                            CompletedPhoto1 = o.CompletedPhoto1,
                                            CompletedPhoto2 = o.CompletedPhoto2,
                                            CompletedPhoto3 = o.CompletedPhoto3
                                        }).First();
                }
                catch
                {
                    throw;
                }
            }
            return orderInformation;
        }

        public static List<Orders.ClaimedOrders> GetProviderClaimedOrders(int providerId)
        {
            List<Orders.ClaimedOrders> claimedOrders = new List<Orders.ClaimedOrders>();
            using (var db = new LawnHiroDB())
            {
                try
                {
                    claimedOrders = (from o in db.Orders
                                     where o.ClaimedBy == providerId && o.Completed != true
                                     select new Orders.ClaimedOrders()
                                     {
                                         OrderID = o.Id,
                                         FirstName = o.CustomerFirstName,
                                         LastName = o.CustomerLastName,
                                         Email = o.CustomerEmail,
                                         Address1 = o.AddressLine1,
                                         Address2 = o.AddressLine2,
                                         City = o.City,
                                         State = o.State,
                                         Zip = o.Zip,
                                         Price = o.Price,
                                         CustomerNotes = o.CustomerNotes,
                                         ETA = o.ETA
                                     }).ToList();
                }
                catch
                {
                    throw;
                }
            }
            return claimedOrders;
        }

        public static List<string> GetCurrentOrderCities()
        {
            List<string> activeCities = new List<string>();
            using (var db = new LawnHiroDB())
            {
                try
                {
                    activeCities = (from o in db.Orders
                                    where o.ClaimedBy == null && o.Completed != true
                                    select o.City).ToList();
                    activeCities = activeCities.Distinct().ToList();
                }
                catch
                {
                    throw;
                }
            }
            return activeCities;
        }
    }
}