﻿<%@ Page Title="Active Jobs - LawnHiro (Partial)" Language="C#" MasterPageFile="~/LawnHiro.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="opendorse_project.Portals.Provider.ActiveOrders.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/Scripts/page-scripts/active-jobs.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATiMiFCSPiuHmHGwBrBhGOEmH3wadkXhM&callback=initMap"></script>

    <link rel="stylesheet" href="/Content/page-css/active-jobs.css" />
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="screen"
        href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css" />

    <script>
        $(function () {
            initialize();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="OrdersDiv" style="margin: 0 auto; padding-top: 50px">
        <div id="FiltersDiv" style="text-align: center">
            <select data-bind="options: citiesFilter, value: selectedCity, valueAllowUnset: true" onchange="getActiveOrders(value)"></select>
        </div>
        <div class="span3">
            <div class="activeOrderListing">
                <div style="text-align: center; display: none; font-style: italic;" data-bind="visible: activeOrders().length === 0">
                    No jobs available :(
                </div>
                <div data-bind="foreach: activeOrders">
                    <div style="border-bottom: 3px solid black; padding: 5px 2px;">
                        <div>
                            <span><b>Order Number:</b></span>
                            <span data-bind="text: OrderID"></span>
                        </div>
                        <div>
                            <span><b>Address:</b></span>
                            <span data-bind="text: Address1 + ','"></span>
                            <span data-bind="text: Address2 + ',', visible: (Address2)"></span>
                            <span data-bind="text: City + ','"></span>
                            <span data-bind="text: State"></span>
                            <span data-bind="text: Zip"></span>
                        </div>
                        <div data-bind="visible: (CustomerNotes)">
                            <span><b>Customer Notes:</b></span>
                            <span data-bind="text: CustomerNotes"></span>
                        </div>
                        <div>
                            <span><b>Your Earnings:</b></span>
                            <span data-bind="text: '$' + ((Price * 0.7).toFixed(2))"></span>
                        </div>
                        <div style="text-align: center">
                            <input type="button" class="btn btn-success" value="Claim" data-bind="click: openClaimJobModal.bind($data, OrderID)" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span12">
        <div id="Maps" class="googleMap">
        </div>
    </div>
    <div id="etaModalDiv" class="modal" style="text-align: center; overflow: hidden; height: 100px; width: 500px; border: 3px solid black; border-radius: 10px; display: none;">
        <span id="closeModal" class="close" onclick="closeClaimJobModal();">&times;</span>
        <div class="modalContent" style="padding: 10px 10px">
            <div>
                When do you estimate to be at this job?
            </div>
            <div id="etaDateTimePicker" class="input-append date">
                <input id="etaDateTimePickerValue" type="text"></input>
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
                <script type="text/javascript">
                    $('#etaDateTimePicker').datetimepicker({
                        format: 'MM/dd/yyyy HH:mm PP',
                        language: 'en',
                        pick12HourFormat: true,
                        pickSeconds: false
                    });
                </script>
            </div>
            <div>
                <input type="button" class="btn btn-primary" id="modalCompleteOrder" value="Finish Claim" onclick="claimJob()" />
            </div>
        </div>
    </div>
    <style>
        .Spinner {
            background: #ffffff;
            opacity: 0.8;
            color: #666666;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
        }
    </style>
    <div id="claimingSpinner" class="Spinner" style="display: none;">
        <img src="/loader.gif" alt="Loading" /><br />
        Claiming Job...
    </div>
</asp:Content>
