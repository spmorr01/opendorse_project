﻿<%@ Page Title="Current Jobs - LawnHiro (Partial)" Language="C#" MasterPageFile="~/LawnHiro.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="opendorse_project.Portals.Provider.CurrentJobs.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Scripts/page-scripts/current-jobs.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATiMiFCSPiuHmHGwBrBhGOEmH3wadkXhM&callback=initMap"></script>
    <script type="text/javascript"
        src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
    </script>
    <script type="text/javascript"
        src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>

    <link rel="stylesheet" href="/Content/page-css/current-jobs.css" />
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="screen"
        href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css" />

    <script>
        $(function () {
            initialize();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin: 0 auto; padding-top: 50px">
        <div class="span3">
            <div id="OrdersDiv" class="activeOrderListing">
                <div style="text-align: center; display: none; font-style: italic;" data-bind="visible: activeOrders().length === 0">
                    No claimed jobs :(
                </div>
                <div data-bind="foreach: activeOrders">
                    <div style="border-bottom: 3px solid black; padding: 5px 2px;">
                        <div>
                            <span><b>Order Number:</b></span>
                            <span data-bind="text: OrderID"></span>
                        </div>
                        <div>
                            <span><b>Your ETA:</b></span>
                            <span data-bind="text: moment(ETA).format('LLL')"></span>
                        </div>
                        <div>
                            <span><b>Address:</b></span>
                            <span data-bind="text: Address1 + ','"></span>
                            <span data-bind="text: Address2 + ',', visible: (Address2)"></span>
                            <span data-bind="text: City + ','"></span>
                            <span data-bind="text: State"></span>
                            <span data-bind="text: Zip"></span>
                        </div>
                        <div data-bind="visible: (CustomerNotes)">
                            <span><b>Customer Notes:</b></span>
                            <span data-bind="text: CustomerNotes"></span>
                        </div>
                        <div>
                            <span><b>Your Earnings:</b></span>
                            <span data-bind="text: '$' + ((Price * 0.7).toFixed(2))"></span>
                        </div>
                        <div style="text-align: center">
                            <input type="button" class="btn btn-success" value="Complete Job" data-bind="click: openCompleteJobModal.bind($data)" />
                            <input type="button" class="btn btn-primary" value="Reschedule" data-bind="click: openRescheduleModal.bind($data)" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span12">
            <div id="Maps" class="googleMap">
            </div>
        </div>
    </div>
    <div id="CompleteOrderModalDiv" class="modal" style="height: 250px; width: 375px; padding-left: 15px; border: 3px solid black; border-radius: 10px; display: none;">
        <span id="closeModal" class="close" onclick="closeCompleteJobModal();">&times;</span>
        <div class="modalContent">
            <div class="divFileUploaders">
                <input id="uploadFile1" placeholder="Choose File" disabled="disabled" />
                <div class="fileUpload btn btn-primary" style="width: 100px">
                    <span>Image #1*</span>
                    <input id="uploadBtn1" type="file" class="upload" onchange="changeDoc1Input();" />
                </div>
            </div>
            <div class="divFileUploaders">
                <input id="uploadFile2" placeholder="Choose File" disabled="disabled" />
                <div class="fileUpload btn btn-primary" style="width: 100px">
                    <span>Image #2*</span>
                    <input id="uploadBtn2" type="file" class="upload" onchange="changeDoc2Input();" />
                </div>
            </div>
            <div class="divFileUploaders">
                <input id="uploadFile3" placeholder="Choose File" disabled="disabled" />
                <div class="fileUpload btn btn-primary" style="width: 100px">
                    <span>Image #3</span>
                    <input id="uploadBtn3" type="file" class="upload" onchange="changeDoc3Input();" />
                </div>
            </div>
            <div style="padding-top: 5px;">
                <span style="font-style: italic">Address:</span>
                <span id="modalCustomerAddressSpan"></span>
            </div>
            <div>
                <span style="vertical-align: top; font-style: italic">Notes to LawnHiro:</span>
                <textarea id="modalProviderNotesInput"></textarea>
            </div>
            <div style="text-align: center; padding-top: 5px;">
                <input type="button" id="modalCompleteOrder" class="btn btn-success" style="width: 150px" value="Finish Order" onclick="validateAndCompleteOrder();" />
            </div>
        </div>
    </div>
    <div id="RescheduleOrderModalDiv" class="modal" style="text-align: center; overflow: hidden; height: 185px; width: 500px; padding-top: 15px; padding-left: 15px; border: 3px solid black; border-radius: 10px; display: none;">
        <span id="closeRescheduleModal" class="close" onclick="closeRescheduleModal();">&times;</span>
        <div class="modalContent" style="padding: 10px 10px">
            <span>Reschedule Time*:
                </span>
            <div id="etaDateTimePicker" class="input-append date">
                
                <input id="newDateTimePickerValue" type="text" />
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
                <script type="text/javascript">
                    $('#etaDateTimePicker').datetimepicker({
                        format: 'MM/dd/yyyy HH:mm PP',
                        language: 'en',
                        pick12HourFormat: true,
                        pickSeconds: false
                    });
                </script>
            </div>
            <div>
                <div style="vertical-align: top; font-style: italic">Reason*:</div>
                <textarea id="modalRescheduleNotes"></textarea>
            </div>
            <div>
                <input type="button" class="btn btn-primary" id="modalReschedule" value="Notify Customer" onclick="rescheduleJob()" />
            </div>
        </div>
    </div>
    <style>
        .Spinner {
            background: #ffffff;
            opacity: 0.8;
            color: #666666;
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 5000;
            top: 0;
            left: 0;
            float: left;
            text-align: center;
            padding-top: 25%;
        }
    </style>
    <div id="uploadingSpinner" class="Spinner" style="display: none;">
        <img src="/loader.gif" alt="Loading" /><br />
        Uploading Files...
    </div>
    <div id="completingSpinner" class="Spinner" style="display: none;">
        <img src="/loader.gif" alt="Loading" /><br />
        Completing Job...
    </div>
    <div id="reschedulingSpinner" class="Spinner" style="display: none;">
        <img src="/loader.gif" alt="Loading" /><br />
        Rescheduling Job...
    </div>
</asp:Content>
