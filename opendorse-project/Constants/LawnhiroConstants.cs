﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.Constants
{
    public class LawnHiroConstants
    {
        // TODO: Major TODO is to move everything over to using constants on the rest of the site. Will be a major project to finish when nothing else major to work on
        public class ApiResponseConstants
        {
            public const string SuccessKey = "success";
            public const string MessageKey = "message";
            public const string DataKey = "data";

            public const string SuccessMessage = "Success!";
            public const string ErrorMessage = "Sorry, an error has occurred. Please try again.";
        }

        public class ProviderOrdersControllerConstants
        {
            public class OrderClaimConstants
            {
                public const string OrderId = "OrderId";
                public const string ETA = "ETA";
            }

            public class RescheduleOrderConstants
            {
                public const string OrderId = "OrderId";
                public const string NewTime = "NewTime";
                public const string Reason = "Reason";
            }

            public class CompleteOrderConstants
            {
                public const string OrderId = "OrderId";
                public const string ProviderNotes = "ProviderNotes";
            }
        }
        public class OrderReceivedEmailNotification
        {
            public const string CustomerName = "###CUSTOMERNAME###";
            public const string CustomerEmail = "###CUSTOMEREMAIL###";
            public const string CustomerNotes = "###CUSTOMERNOTES###";
            public const string StreetAddress = "###STREETADDRESS###";
            public const string City = "###CITY###";
            public const string State = "###STATE###";
            public const string Zip = "###ZIP###";
            public const string OrderPrice = "###ORDERPRICE###";
            public const string OrderDate = "###ORDERDATE###";
            public const string TransactionID = "###TRANSACTIONID###";
        }

        public class OrderReceivedForProviderEmailNotification
        {
            public const string CityStateHeader = "###CITYSTATEHEADER###";
            public const string StreetAddress = "###STREETADDRESS###";
            public const string CityStateZip = "###CITYSTATEZIP###";
            public const string Earnings = "###EARNINGS###";
            public const string OrderNotes = "###ORDERNOTES###";
            public const string StaticMapImage = "###STATICMAPIMAGE###";
        }
        public class ClaimOrderEmailNotification
        {
            public const string ETA = "###ETA###";
            public const string LawnhiroInfo = "###LAWNHIROINFO###";
            public const string DisplayPhotoId = "###DISPLAYPHOTOID###";
            public const string MowerId = "###MOWERID###";
        }

        public class OrderReceivedAreas
        {
            public static readonly List<string> LincolnArea = new List<string>() { "Lincoln", "Eagle", "Hickman", "Waverly" };

            public static readonly List<string> OmahaArea = new List<string>() { "Omaha", "Bellevue", "La Vista", "Papillion", "Ralston", "Millard", "Chalco", "Carter Lake", "Gretna", "Bennington", "Elkhorn" };
        }
    }
}