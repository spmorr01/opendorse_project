﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using static opendorse_project.Entities.LawnHiroTables;

namespace opendorse_project.Entities
{
    public partial class LawnHiroDB : DbContext
    {
        public LawnHiroDB() : base("name=LawnHiroConnectionString")
        { }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<CustomerResidence> CustomerResidence { get; set; }
        public virtual DbSet<Residence> Residence { get; set; }
        public virtual DbSet<Provider> Provider { get; set; }
        public virtual DbSet<HeardAboutUsSource> HeardAboutUsSource { get; set; }
        public virtual DbSet<CouponCodes> CouponCodes { get; set; }
        public virtual DbSet<CouponsUsed> CouponsUsed { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
        }



    }
}