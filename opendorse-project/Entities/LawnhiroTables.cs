﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace opendorse_project.Entities
{
    public class LawnHiroTables
    {
        [Table("Customer")]
        public class Customer
        {
            [Key]
            public int Id { get; set; }

            public string Email { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }
        }

        [Table("Residence")]
        public class Residence
        {
            [Key]
            public int Id { get; set; }

            public string AddressLine1 { get; set; }

            public string AddressLine2 { get; set; }

            public string City { get; set; }

            public string State { get; set; }

            public string Zip { get; set; }

            public string GooglePlaceId { get; set; }

            public string ProviderCode { get; set; }

            [ForeignKey("HeardAboutUsSource")]
            public int HeardAboutUsSourceId { get; set; }

            public float MowableSqFeet { get; set; }

            public decimal PriceOverride { get; set; }

            public HeardAboutUsSource HeardAboutUsSource { get; set; }
        }

        [Table("CustomerResidence")]
        public class CustomerResidence
        {
            [Key, ForeignKey("Customer"), Column(Order = 1)]
            public int CustomerId { get; set; }

            [Key, ForeignKey("Residence"), Column(Order = 2)]
            public int ResidenceId { get; set; }

            public Customer Customer { get; set; }

            public Residence Residence { get; set; }
        }

        [Table("HeardAboutUsSource")]
        public class HeardAboutUsSource
        {
            [Key]
            public int Id { get; set; }

            public string Name { get; set; }
        }

        [Table("Providers")]
        public class Provider
        {
            [Key]
            public int Id { get; set; }

            public string Email { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string AddressLine1 { get; set; }

            public string AddressLine2 { get; set; }

            public string City { get; set; }

            public string State { get; set; }

            public string Zip { get; set; }

            public string PhoneNumber { get; set; }

            public byte[] PasswordSalt { get; set; }

            public byte[] PasswordHash { get; set; }

            public bool Active { get; set; }

            public bool Admin { get; set; }

            [ForeignKey("CouponCodes")]
            public string CouponCode { get; set; }

            public string PayPalEmail { get; set; }

            public bool Confirmed { get; set; }

            public byte[] Headshot { get; set; }

            public byte[] Mower { get; set; }

            public DateTime SignedAgreement { get; set; }

            public CouponCodes CouponCodes { get; set; }
        }

        [Table("Orders")]
        public class Orders
        {
            [Key]
            public int Id { get; set; }

            public string CustomerFirstName { get; set; }

            public string CustomerLastName { get; set; }

            public string CustomerEmail { get; set; }

            public string AddressLine1 { get; set; }

            public string AddressLine2 { get; set; }

            public string City { get; set; }

            public string State { get; set; }

            public string Zip { get; set; }

            public DateTime Placed { get; set; }

            public string PayPalOrderId { get; set; }

            public string BusinessSource { get; set; }

            public double CalculatedArea { get; set; }

            public decimal Price { get; set; }

            public DateTime? ETA { get; set; }

            public string CustomerNotes { get; set; }

            [ForeignKey("Provider")]
            public int? ClaimedBy { get; set; }

            public bool Completed { get; set; }

            public string ProviderNotes { get; set; }

            public double? CustomerRating { get; set; }

            public bool ProviderPaid { get; set; }

            public DateTime? DateCompleted { get; set; }

            public byte[] CompletedPhoto1 { get; set; }

            public byte[] CompletedPhoto2 { get; set; }

            public byte[] CompletedPhoto3 { get; set; }

            public string CouponUsed { get; set; }

            public Provider Provider { get; set; }
        }

        [Table("CouponCodes")]
        public class CouponCodes
        {
            [Key]
            public string Code { get; set; }

            public int TimesUsed { get; set; }

            public bool Enabled { get; set; }

            public decimal Discount { get; set; }
        }

        [Table("CouponsUsed")]
        public class CouponsUsed
        {
            [Key]
            public int Id { get; set; }

            public string CustomerEmail { get; set; }

            public string CouponCode { get; set; }
        }
    }
}