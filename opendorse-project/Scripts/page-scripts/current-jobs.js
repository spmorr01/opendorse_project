﻿// TODO: create var to act as a class
var toastOptions = {
    "positionClass": "toast-top-full-width",
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

var map,
    completeOrderModal, rescheduleOrderModal, closeModal,
    modalOrderNumber, modalCustomer, modalAddress, modalProviderNotes,
    globalOrderId,
    file1, file2, file3,
    newDateTimePickerValue, finishOrderButton,
    uploadingSpinner, completingSpinner, reschedulingSpinner;

function initMap() {
    //Waiting for document to be finished loading before creating map
}

function createMap() {
    var defaultLatLng = { lat: 40.8258, lng: -96.6852 };

    map = new google.maps.Map(document.getElementById('Maps'), {
        zoom: 12,
        center: defaultLatLng
    });
    map.setTilt(45);

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(pos);
        }, function () {
            map.setCenter(defaultLatLng);
        });
    } else {
        map.setCenter(defaultLatLng);
    }
}

function addMarkers(lat, lng, orderId) {
    var position = { lat: lat, lng: lng };
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(position);
    marker = new google.maps.Marker({
        position: position,
        map: map,
        zoomControl: true,
        scaleControl: true,
        title: orderId
    });
}

var activeOrderVm = {
    activeOrders: ko.observableArray()
}

var addresses = [];
var latLngList = [];

function initialize() {
    completeOrderModal = document.getElementById("CompleteOrderModalDiv");
    rescheduleOrderModal = document.getElementById("RescheduleOrderModalDiv");
    closeModal = document.getElementById("closeModal");
    modalAddress = document.getElementById("modalCustomerAddressSpan");
    modalProviderNotes = document.getElementById("modalProviderNotesInput");

    file1 = document.getElementById('uploadBtn1');
    file2 = document.getElementById('uploadBtn2');
    file3 = document.getElementById('uploadBtn3');

    newDateTimePickerValue = document.getElementById('newDateTimePickerValue');

    finishOrderButton = document.getElementById('modalCompleteOrder');
    uploadingSpinner = document.getElementById('uploadingSpinner');
    completingSpinner = document.getElementById('completingSpinner');
    reschedulingSpinner = document.getElementById('reschedulingSpinner');
    finishOrderButton = document.getElementById('modalCompleteOrder');

    getClaimedOrders();
    ko.applyBindings(activeOrderVm, document.getElementById("OrdersDiv"));
    ko.applyBindings(activeOrderVm, completeOrderModal);
    createMap();
}

function openCompleteJobModal(data) {
    modalAddress.textContent = data.Address1 + ', ' + ((data.Address2) ? data.Address2 + ', ' : '') + data.City + ', ' + data.State + ' ' + data.Zip;
    completeOrderModal.style.display = "block";
    globalOrderId = data.OrderID;
}

function closeCompleteJobModal() {
    completeOrderModal.style.display = "none";
}

function openRescheduleModal(data) {
    rescheduleOrderModal.style.display = "block";
    globalOrderId = data.OrderID;
}

function closeRescheduleModal() {
    rescheduleOrderModal.style.display = "none";
}

function getClaimedOrders() {
    // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files 
    $.ajax({
        url: '/api/ProviderOrders/GetClaimedOrders',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',

        success: function (response) {
            if (response.data.success) {
                activeOrderVm.activeOrders(response.data.data);
                ko.utils.arrayForEach(activeOrderVm.activeOrders(), function (values) {
                    var address = values.Address1 + ', ' + values.City + ', ' + values.State + ' ' + values.Zip
                    var orderId = String(values.OrderID);
                    getGeoCodingInfo(address, orderId);
                });

            } else {
                toastr.error('An unexpected error occurred. Try refreshing the page, or contact LawnHiro if your issues persist.', 'Error', toastOptions);
            }
        },
        fail: function (jqXHR, textStatus) {
            toastr.error('An unexpected error occurred. Please contact LawnHiro if it persists.', 'Error', toastOptions);
        }
    })
}

function getGeoCodingInfo(addressParam, orderId) {
    // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files 
    $.ajax({
        url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + addressParam + '&key=AIzaSyATiMiFCSPiuHmHGwBrBhGOEmH3wadkXhM',
        type: 'GET',
        dataType: 'json',

        success: function (data) {
            var lat = data.results[0].geometry.location.lat;
            var lng = data.results[0].geometry.location.lng;

            addMarkers(lat, lng, orderId);
        },
        fail: function (jqXHR, textStatus) {
            toastr.error('An unexpected error occurred. Please contact LawnHiro if it persists.', 'Error', toastOptions);
        }
    })
}

function validateAndCompleteOrder() {
    if (file1.files.length === 0 || file2.files.length === 0) {
        toastr.info('Need to add at least two images.', 'Images Needed', toastOptions);
    } else {
        if (file1.files[0].type.indexOf("image/") === -1) {
            toastr.info('Please select a valid image file on image #1', 'Invalid Image', toastOptions);
        } else if (file2.files[0].type.indexOf("image/") === -1) {
            toastr.info('Please select a valid image file on image #2', 'Invalid Image', toastOptions);
        } else if (file3.files.length > 0 && file3.files[0].type.indexOf("image/") === -1) {
            toastr.info('Please select a valid image file on image #3', 'Invalid Image', toastOptions);
        } else {
            CompleteOrder();
        }
    }
}

function CompleteOrder() {
    completingSpinner.style.display = 'inherit';

    var OrderData = new FormData();
    OrderData.append('OrderId', globalOrderId);
    OrderData.append('File1', file1.files[0]);
    OrderData.append('File2', file2.files[0]);
    OrderData.append('File3', file3.files.length > 0 ? file3.files[0] : null);
    OrderData.append('ProviderNotes', modalProviderNotes.value);

    // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files 
    $.ajax({
        url: '/api/ProviderOrders/CompleteOrder',
        processData: false,
        contentType: false,
        data: OrderData,
        type: 'Put',

        success: function (response) {
            if (response.data.success) {
                toastr.success('You completed order number: ' + globalOrderId, 'Completed Job', toastOptions);
                completingSpinner.style.display = 'none';
                completeOrderModal.style.display = "none";
                createMap();
                getClaimedOrders();
            } else {
                toastr.error('Unable to complete order number: ' + globalOrderId, 'Error', toastOptions);
            }
        },
        fail: function (jqXHR, textStatus) {
            toastr.error('An unexpected error occurred. Please contact LawnHiro if it persists.', 'Error', toastOptions);
        }
    })
}

function changeDoc1Input() {
    var fileUploader = document.getElementById("uploadBtn1");
    document.getElementById("uploadFile1").value = fileUploader.value.split(/(\\|\/)/g).pop();
}

function changeDoc2Input() {
    var fileUploader = document.getElementById("uploadBtn2");
    document.getElementById("uploadFile2").value = fileUploader.value.split(/(\\|\/)/g).pop();
}

function changeDoc3Input() {
    var fileUploader = document.getElementById("uploadBtn3");
    document.getElementById("uploadFile3").value = fileUploader.value.split(/(\\|\/)/g).pop();
}

function rescheduleJob() {
    reschedulingSpinner.style.display = 'inherit';

    if (modalRescheduleNotes.value !== '' && newDateTimePickerValue.value !== '') {

        var RescheduleData = new FormData();
        RescheduleData.append('OrderId', globalOrderId);
        RescheduleData.append('NewTime', newDateTimePickerValue.value);
        RescheduleData.append('Reason', modalRescheduleNotes.value);

        // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files 
        $.ajax({
            url: '/api/ProviderOrders/RescheduleOrder',
            processData: false,
            contentType: false,
            data: RescheduleData,
            type: 'Put',

            success: function (response) {
                if (response.data.success) {
                    toastr.success('You rescheduled order number: ' + globalOrderId, 'Rescheduled Order', toastOptions);
                    reschedulingSpinner.style.display = 'none';
                    rescheduleOrderModal.style.display = "none";
                    getClaimedOrders();
                } else {
                    toastr.error('Failed to reschedule order number: ' + globalOrderId, 'Error', toastOptions);
                }
            },
            fail: function (jqXHR, textStatus) {
                toastr.error('An unexpected error occurred. Please contact LawnHiro if it persists.', 'Error', toastOptions);
            }
        })
    } else {
        reschedulingSpinner.style.display = 'none';
        toastr.info('Please fill out the required values.' + globalOrderId, 'Required Items Needed', toastOptions);
    }
}