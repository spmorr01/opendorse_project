﻿// TODO: Pretty gross file here too. Need to clean it up
// TODO: add var to make it a class

// TODO: Find the unreferenced variables out of here. Switched from global variables to KO view models
var loadingSpinner, autocomplete, addressInput, orderModal, calculatedPriceSpan,
    termsCheckbox, orderFirstName, orderLastName, orderEmail, orderCustomerNotes,
    orderBusinessSource, orderAmount, calculatedResultArea, calculatedResultPrice,
    address, city, state, zip, globalActions;

var toastOptions = {
    "positionClass": "toast-top-full-width",
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
var rootVM = {
    orderVM: {
        finalPrice: ko.observable(0),
        couponCode: ko.observable(),
        couponUsed: ko.observable(false),
        calculatedArea: ko.observable(),
        calculatedPrice: ko.observable(),
        firstName: ko.observable(),
        lastName: ko.observable(),
        email: ko.observable(),
        address: ko.observable(),
        city: ko.observable(),
        state: ko.observable(),
        zip: ko.observable(),
        businessSource: ko.observable(),
        payPalOrderID: ko.observable(),
        customerNotes: ko.observable()
    }
}


function Initialize() {
    loadingSpinner = document.getElementById('loadingSpinner');
    loadingSpinner.style.display = 'inherit';

    google.maps.event.addDomListener(window, 'load', initializeGoogleAutocomplete);

    // TODO: Need to move to JQuery selectors
    termsCheckbox = document.getElementById('termsAndConditions');
    orderModal = document.getElementById('PayPalModalDiv');
    calculatedPriceSpan = document.getElementById('orderAmount');
    orderFirstName = document.getElementById('orderFirstName');
    orderLastName = document.getElementById('orderLastName');
    orderEmail = document.getElementById('orderEmail');
    orderCustomerNotes = document.getElementById('orderCustomerNotes');
    orderBusinessSource = document.getElementById('orderBusinessSource');

    paypal.Button.render(
        {
            env: 'sandbox', // Specify 'sandbox' for the test environment, 'production'

            client: {
                sandbox: 'AeL5Z6IirMijkry6LzbZ8aS9E47B0AH2tHizjdJxrvMprG6X93w7w5I1zjJYQsOkYKzF0ZWLt5CcpkJ-'
            },

            validate: function (actions) {
                toggleButton(actions);

                onChangeCheckbox(function () {
                    toggleButton(actions);
                });
            },

            onClick: function () {
                toggleValidationMessage();
            },

            payment: function () {
                var env = this.props.env;
                var client = this.props.client;

                return paypal.rest.payment.create(env, client, {
                    transactions: [
                        {
                            amount: { total: ko.unwrap(rootVM.orderVM.finalPrice), currency: 'USD' }
                        }
                    ]
                });
            },

            commit: true,

            onAuthorize: function (data, actions) {
                // Execute the payment here, when the buyer approves the transaction
                return actions.payment.execute().then(function () {
                    sendOrderToLawnHiro(data.paymentID);
                });
            }

        }, '#paypal-button');

    ko.applyBindings(rootVM);
}

function validateStep1() {
    return orderFirstName.value !== '' && orderLastName.value !== '' && orderEmail.value !== '';
}

function isValid() {
    return document.querySelector('#termsAndConditions').checked;
}

function onChangeCheckbox(handler) {
    document.querySelector('#termsAndConditions').addEventListener('change', handler);
}

function toggleValidationMessage() {
    if (!isValid()) toastr.info('Please agree to the terms and conditions.', 'Whoops!', toastOptions);
}

function toggleButton(actions) {
    return isValid() ? actions.enable() : actions.disable();
}

function initializeGoogleAutocomplete() {
    addressInput = document.getElementById('addressInput');
    autocomplete = new google.maps.places.Autocomplete(addressInput);
    loadingSpinner.style.display = 'none';
}

function addOrder() {
    loadingSpinner.style.display = 'inherit';
    var addressData = autocomplete.getPlace().address_components;

    var tempStreetNumber, tempRoute;

    $.each(addressData, function(index, addressDataItem) {
        $.each(addressDataItem.types, function (index, typeItem) {
            if (typeItem === "street_number") {
                tempStreetNumber = addressDataItem.long_name;
            } else if (typeItem === "route") {
                tempRoute = addressDataItem.long_name;
            } else if (typeItem === "locality") {
                rootVM.orderVM.city(addressDataItem.long_name);
            } else if (typeItem === "administrative_area_level_1") {
                rootVM.orderVM.state(addressDataItem.short_name);
            } else if (typeItem === "postal_code") {
                rootVM.orderVM.zip(addressDataItem.long_name);
            }
        })
    })
   
    rootVM.orderVM.address(tempStreetNumber + ' ' + tempRoute);

    getZillowData(rootVM.orderVM);
}

function getZillowData(orderVM) {
    var ZillowAddress = new Object();
    ZillowAddress.Address = ko.unwrap(orderVM.address);
    ZillowAddress.City = ko.unwrap(orderVM.city);
    ZillowAddress.Ctate = ko.unwrap(orderVM.state);
    ZillowAddress.Zip = ko.unwrap(orderVM.zip);

    // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files
    $.ajax({
        url: window.location.origin + "/api/LocationData/GetOrderInfoForLocation",
        type: 'Post',
        dataType: 'json',
        data: JSON.stringify(ZillowAddress),
        contentType: 'application/json; charset=utf-8',

        success: function (response) {
            if (response.data.success) {
                openOrderModal(response.data.data);
            } else {
                toastr.error('Oops. Something went wrong. Please try again, or contact LawnHiro if the problem persists.', 'Error', toastOptions);
            }
        },
        fail: function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        }
    })
}

function openOrderModal(zillowData) {
    rootVM.orderVM.calculatedArea(zillowData.CalculatedResultArea);
    rootVM.orderVM.finalPrice(zillowData.CalculatedResultPrice);
    rootVM.orderVM.calculatedPrice(zillowData.CalculatedResultPrice);

    if (ko.unwrap(rootVM.orderVM.calculatedPrice) === 0) {
        loadingSpinner.style.display = 'none';
        toastr.error('We were unable to find data for your address. Please try again, or reach out to LawnHiro if your error persists.', 'Bad Address', toastOptions);
    } else {
        loadingSpinner.style.display = 'none';
        $('#btnLaunchOrderModal').click();

    }
}

function closeOrderModal() {
    $('.js-btn-step[data-orientation=cancel]').click();
}

function sendOrderToLawnHiro(payPalOrderId) {
    loadingSpinner.style.display = 'inherit';

    var OrderData = new Object();
    OrderData.FirstName = ko.unwrap(rootVM.orderVM.firstName);
    OrderData.LastName = ko.unwrap(rootVM.orderVM.lastName);
    OrderData.Email = ko.unwrap(rootVM.orderVM.email);
    OrderData.Address1 = ko.unwrap(rootVM.orderVM.address);
    OrderData.City = ko.unwrap(rootVM.orderVM.city);
    OrderData.State = ko.unwrap(rootVM.orderVM.state);
    OrderData.Zip = ko.unwrap(rootVM.orderVM.zip);
    OrderData.BusinessSource = ko.unwrap(rootVM.orderVM.businessSource);
    OrderData.PayPalOrderID = payPalOrderId;
    OrderData.CalculationArea = ko.unwrap(rootVM.orderVM.calculatedArea);
    OrderData.Price = ko.unwrap(rootVM.orderVM.calculatedPrice);
    OrderData.CustomerNotes = ko.unwrap(rootVM.orderVM.customerNotes);
    OrderData.CouponUsed = ko.unwrap(rootVM.orderVM.couponUsed);
    OrderData.CouponCode = ko.unwrap(rootVM.orderVM.couponCode);

    // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files
    $.ajax({
        url: '/api/CustomerOrders/AddNewOrder',
        type: 'POST',
        data: JSON.stringify(OrderData),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',

        success: function (response) {
            loadingSpinner.style.display = 'none';

            if (response.data.success) {
                toastr.success('Your order has been received! Stay tuned for updates to your order via email.', 'Order Received', toastOptions);
                addressInput.value = '';
                resetVM();
                closeOrderModal();

            } else {
                toastr.error('Oops. Something went wrong. Please try again, or contact LawnHiro if the problem persists.', 'Error', toastOptions);
            }
        },
        fail: function (jqXHR, textStatus) {
            toastr.error('Unexpected error occurred. Please try again.', 'Oops.');
        }
    })
}

function resetVM() {
    rootVM.orderVM.finalPrice(0);
    rootVM.orderVM.couponCode('');
    rootVM.orderVM.couponUsed(false);
    rootVM.orderVM.calculatedArea('');
    rootVM.orderVM.calculatedPrice(0);
    rootVM.orderVM.firstName('');
    rootVM.orderVM.lastName('');
    rootVM.orderVM.email('');
    rootVM.orderVM.address('');
    rootVM.orderVM.city('');
    rootVM.orderVM.state('');
    rootVM.orderVM.zip('');
    rootVM.orderVM.businessSource('');
    rootVM.orderVM.payPalOrderID('');
    rootVM.orderVM.customerNotes('');
}

function applyCouponCode(data) {
    loadingSpinner.style.display = 'inherit';

    if (ko.unwrap(data.orderVM.email) == null || ko.unwrap(data.orderVM.couponCode) == null) {
        loadingSpinner.style.display = 'none';
        toastr.info('Please provide the required information to apply your coupon code.', 'More Information Needed', toastOptions);
    } else {
        var CouponCode = new FormData();
        CouponCode.append("CustomerEmail", ko.unwrap(data.orderVM.email));
        CouponCode.append("CouponCode", ko.unwrap(data.orderVM.couponCode));
        CouponCode.append("Price", ko.unwrap(data.orderVM.calculatedPrice));

        // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files
        $.ajax({
            url: '/api/CustomerOrders/ApplyCoupon',
            processData: false,
            contentType: false,
            data: CouponCode,
            type: 'POST',

            success: function (result) {
                loadingSpinner.style.display = 'none';
                console.log(data);
                if (result.data.success) {
                    if (result.data.data.Valid) {
                        data.orderVM.finalPrice(result.data.data.NewPrice);
                        data.orderVM.couponUsed(true);
                        toastr.success('Successfully applied coupon to your order.', 'Coupon Found', toastOptions);
                    } else {
                        toastr.info('That is not a valid coupon, or you have already used it in the past. Please try agin.', 'Coupon Not Found', toastOptions);
                    }
                } else {
                    toastr.error('Oops. Something went wrong. Please try again, or contact LawnHiro if the problem persists.', 'Error', toastOptions);
                }
            },
            fail: function (jqXHR, textStatus) {
                toastr.error('Unexpected error occurred. Please try again.', 'Oops.');
            }
        })
    }
}