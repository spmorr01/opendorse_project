﻿// TODO: create var to act as a class
var toastOptions = {
    "positionClass": "toast-top-full-width",
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

var map, globalOrderID, claimJobModal, etaDateTimePicker, claimingSpinner;

function initMap() {
    //Waiting for document to be finished loading before creating map
}

function createMap() {
    var defaultLatLng = { lat: 40.8258, lng: -96.6852 }; // Lincoln

    map = new google.maps.Map(document.getElementById('Maps'), {
        zoom: 12,
        center: defaultLatLng
    });
    map.setTilt(45);


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(pos);
        }, function () {
            map.setCenter(defaultLatLng);
        });
    } else {
        map.setCenter(defaultLatLng);
    }
}

function addMarkers(lat, lng, orderId) {
    var position = { lat: lat, lng: lng };
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(position);
    marker = new google.maps.Marker({
        position: position,
        map: map,
        zoomControl: true,
        scaleControl: true,
        title: orderId
    });
}

var activeOrderVm = {
    activeOrders: ko.observableArray(),
    citiesFilter: ko.observableArray(),
    selectedCity: ko.observable()
}

var addresses = [];
var latLngList = [];

function initialize() {
    // TODO: move these to jquery selectors
    claimJobModal = document.getElementById("etaModalDiv");
    etaDateTimePicker = document.getElementById("etaDateTimePickerValue");
    claimingSpinner = document.getElementById("claimingSpinner");
    getActiveOrders();
    ko.applyBindings(activeOrderVm, document.getElementById("OrdersDiv"));
    createMap();
}

function openClaimJobModal(orderId) {
    // TODO: Use KO view models instead of global variable
    globalOrderID = orderId;
    claimJobModal.style.display = "block";
}

function closeClaimJobModal() {
    claimJobModal.style.display = "none";
}

function claimJob() {
    claimingSpinner.style.display = 'inherit';

    if (etaDateTimePicker.value === '') {
        toastr.info('Please select an Estimated Time of Arrival.', 'Invalid ETA', toastOptions);
        claimingSpinner.style.display = 'none';
    } else {
        var data = new FormData();
        data.append("OrderId", globalOrderID);
        data.append("ETA", etaDateTimePicker.value)

        // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files 
        $.ajax({
            url: '/api/ProviderOrders/ClaimOrder/',
            type: 'PUT',
            processData: false,
            contentType: false,
            data: data,

            success: function (response) {
                if (response.data.success) {
                    toastr.success('You have successfully claimed order number: ' + globalOrderID, 'Job Claimed', toastOptions);
                    claimingSpinner.style.display = 'none';
                    closeClaimJobModal();
                    createMap();
                    getActiveOrders();
                } else {
                    toastr.error('Could not claim order: ' + globalOrderID + ' -- It may have been already claimed.', 'Error', toastOptions);
                    createMap();
                    getActiveOrders();
                }
            },
            fail: function (jqXHR, textStatus) {
                toastr.error('An unexpected error occurred. Please contact LawnHiro if it persists.', 'Error', toastOptions);
            }
        })
    }
}

function getActiveOrders(filter) {
    // TODO: Major Priority -- Really need to create a WebApiManager JS file to handle AJAX calls so they are standard and take up way less space in these files 

    $.ajax({
        url: '/api/ProviderOrders/GetActiveOrders?cityFilter=' + filter,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',

        success: function (response) {
            activeOrderVm.citiesFilter(response.data.data.OrderCities);
            activeOrderVm.activeOrders(response.data.data.Orders);
            activeOrderVm.selectedCity(response.data.data.SelectedCity);
            ko.utils.arrayForEach(activeOrderVm.activeOrders(), function (values) {
                var address = values.Address1 + ', ' + ((values.Address2) ? values.Address2 + ', ' : '') + values.City + ', ' + values.State + ' ' + values.Zip
                var orderId = String(values.OrderID);
                getGeoCodingInfo(address, orderId);
            });
        },
        fail: function (jqXHR, textStatus) {
            toastr.error('An unexpected error occurred. Please contact LawnHiro if it persists.', 'Error', toastOptions);
        }
    })
}

function getGeoCodingInfo(addressParam, orderId) {
    $.ajax({
        url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + addressParam + '&key=AIzaSyATiMiFCSPiuHmHGwBrBhGOEmH3wadkXhM',
        type: 'GET',
        dataType: 'json',

        success: function (data) {
            var lat = data.results[0].geometry.location.lat;
            var lng = data.results[0].geometry.location.lng;

            addMarkers(lat, lng, orderId);
        },
        fail: function (jqXHR, textStatus) {
            toastr.error('An unexpected error occurred. Please contact LawnHiro if it persists.', 'Error', toastOptions);
        }
    })
}
