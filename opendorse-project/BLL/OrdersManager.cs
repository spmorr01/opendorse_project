﻿using opendorse_project.Constants;
using opendorse_project.DAL;
using opendorse_project.Global;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.BLL
{
    public class OrdersManager
    {
        public static Orders.ClaimInformationForOrder SetClaimInformationForOrderFromAPI(HttpRequest httpRequest)
        {
            Orders.ClaimInformationForOrder claimOrderInfo = new Orders.ClaimInformationForOrder
            {
                OrderId = Int32.Parse(httpRequest.Form[LawnHiroConstants.ProviderOrdersControllerConstants.OrderClaimConstants.OrderId]),
                ETA = DateTime.Parse(httpRequest.Form[LawnHiroConstants.ProviderOrdersControllerConstants.OrderClaimConstants.ETA])
            };

            return claimOrderInfo;
        }

        public static Orders.RescheduleInformationForOrder SetReschedulenformationForOrderFromAPI(HttpRequest httpRequest)
        {
            Orders.RescheduleInformationForOrder rescheduleInfo = new Orders.RescheduleInformationForOrder
            {
                OrderId = Int32.Parse(httpRequest.Form[LawnHiroConstants.ProviderOrdersControllerConstants.RescheduleOrderConstants.OrderId]),
                NewTime = DateTime.Parse(httpRequest.Form[LawnHiroConstants.ProviderOrdersControllerConstants.RescheduleOrderConstants.NewTime]),
                Reason = httpRequest.Form[LawnHiroConstants.ProviderOrdersControllerConstants.RescheduleOrderConstants.Reason]
            };

            return rescheduleInfo;
        }

        public static Orders.CompletedInformationForOrder SetCompleteInformationForOrderFromAPI(HttpRequest httpRequest)
        {
            Orders.CompletedInformationForOrder completeOrderInfo = new Orders.CompletedInformationForOrder
            {
                OrderId = Int32.Parse(httpRequest.Form[LawnHiroConstants.ProviderOrdersControllerConstants.CompleteOrderConstants.OrderId]),
                File1 = ImageHandler.GetImageBytes(httpRequest.Files[0]),
                File2 = ImageHandler.GetImageBytes(httpRequest.Files[1]),
                File3 = httpRequest.Files.Count > 2 ? ImageHandler.GetImageBytes(httpRequest.Files[2]) : null,
                ProviderNotes = httpRequest.Form[LawnHiroConstants.ProviderOrdersControllerConstants.CompleteOrderConstants.ProviderNotes]
            };

            return completeOrderInfo;
        }

        public static bool AddNewOrder(Orders.NewOrder newOrderData)
        {
            if (AccessOrders.AddNewOrder(newOrderData))
            {
                EmailHandler.EmailReceivedNotification(newOrderData);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IEnumerable<Orders.ActiveOrders> GetActiveOrders(string city)
        {
            return AccessOrders.GetActiveOrders(city);
        }

        public static IEnumerable<Orders.ClaimedOrders> GetProviderClaimedOrders(int providerId)
        {
            return AccessOrders.GetProviderClaimedOrders(providerId);
        }

        public static bool ClaimOrder(int orderId, int providerId, DateTime eta)
        {
            return AccessOrders.Claim(orderId, providerId, eta);
        }

        public static bool IsOrderClaimed(int orderId)
        {
            return AccessOrders.IsOrderClaimed(orderId);
        }

        public static bool CompleteOrder(Orders.CompletedInformationForOrder completeOrderInfo)
        {
            return AccessOrders.CompleteOrder(completeOrderInfo);
        }

        public static Orders.ClaimedOrders GetClaimedOrderInfomation(int orderId)
        {
            return AccessOrders.GetClaimedOrderInfomation(orderId);
        }

        public static Orders.CompletedOrders GetCompletedOrderInfomation(int orderId)
        {
            return AccessOrders.GetCompletedOrderInfomation(orderId);
        }

        public static bool RescheduleOrder(Orders.RescheduleInformationForOrder rescheduleOrderInfo)
        {
            return AccessOrders.RescheduleOrder(rescheduleOrderInfo);
        }
    }
}