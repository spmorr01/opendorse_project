﻿using opendorse_project.DAL;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.BLL
{
    public class CouponManager
    {
        public static Models.CouponCodes.AppliedCouponCode ApplyCoupon(string customerEmail, string couponCode, decimal price)
        {
            CouponCodes.AppliedCouponCode appliedCoupon = new CouponCodes.AppliedCouponCode();
            if (AccessCoupons.CustomerUsedCoupon(customerEmail, couponCode))
            {
                appliedCoupon.NewPrice = 0;
                appliedCoupon.Valid = false;
            }
            else
            {
                CouponCodes.CouponCode coupon = AccessCoupons.GetCouponInfo(couponCode);

                if (coupon == null || coupon.Enabled == false)
                {
                    appliedCoupon.NewPrice = 0;
                    appliedCoupon.Valid = false;
                }
                else
                {
                    appliedCoupon.NewPrice = (price - coupon.Discount);
                    appliedCoupon.Valid = true;
                }
            }
            return appliedCoupon;
        }
    }
}