﻿using opendorse_project.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.BLL
{
    public class FiltersManager
    {
        public static List<string> GetCurrentOrderCities()
        {
            List<string> cities = new List<string>();
            cities.Add("All Cities");
            cities.AddRange(AccessOrders.GetCurrentOrderCities());
            return cities;
        }
    }
}