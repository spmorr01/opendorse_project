﻿using opendorse_project.Constants;
using opendorse_project.DAL;
using opendorse_project.Global;
using opendorse_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace opendorse_project.BLL
{
    public class ProviderManager
    {
        public static Providers.ProviderInfo GetProviderInformation(int providerId)
        {
            return AccountAccess.GetProviderInformation(providerId);
        }

        public static List<Providers.EmailReceivedNotifications> GetProvidersToSendOrderNotificationTo(string city)
        {
            List<string> lincolnArea = LawnHiroConstants.OrderReceivedAreas.LincolnArea;
            List<string> omahaArea = LawnHiroConstants.OrderReceivedAreas.OmahaArea;
            List<string> providerCities = new List<string>();

            if (lincolnArea.Contains(city))
            {
                providerCities = lincolnArea;
            }
            else if (omahaArea.Contains(city))
            {
                providerCities = omahaArea;
            }
            else
            {
                providerCities.Add(city);
            }

            return AccountAccess.GetProvidersToSendOrderNotificationTo(providerCities);
        }
    }
}